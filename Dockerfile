FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

ARG VERSION=1.1.7

RUN mkdir -p /app/code && chown -R cloudron:cloudron /app/code
WORKDIR /app/code

RUN curl -L https://github.com/ad3m3r5/scratch-map/archive/${VERSION}.tar.gz | tar -xz --strip-components 1 -f - -C /app/code

RUN npm install

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]