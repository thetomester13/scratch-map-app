# Scratch Map Cloudron App

This repository contains the Cloudron app package source for [Scratch Map](https://github.com/ad3m3r5/scratch-map).

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd scratch-map-app

cloudron build
cloudron install
```
