#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

export DBLOCATION=/app/data/

echo "==> Starting scratch-map"

exec /usr/local/bin/gosu cloudron:cloudron npm start
